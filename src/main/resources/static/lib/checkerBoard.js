    //全局变量区----start
    var whatSideColor = null
    var isAllowedPlayChess = false
    // var whatSideColor = "white"
    var allPointX = []
    var allPointY = []
    var message = {
        whichUser:null,
        enemyUser:null,
        type:null,
        chessPoint:null,
        playChessInfo:{
            x:null,
            y:null
        }
    }
    var canvas = new fabric.Canvas('canvas');
    //全局变量区----end
    //随机生成UUID

    // var uuid = $("ID").val();

    // var websocket = new WebSocket("ws://127.0.0.1:18080/goBang/"+uuid);
    var websocket = new WebSocket("ws://47.94.160.199:18080/goBang/"+uuid);
    //连接成功建立的回调方法
    websocket.onopen = function(event) {
        
    }
    // let json = '{"version":"4.6.0","objects":[{"type":"rect","version":"4.6.0","originX":"left","originY":"top","left":160,"top":161,"width":80,"height":80,"fill":"red","stroke":null,"strokeWidth":1,"strokeDashArray":null,"strokeLineCap":"butt","strokeDashOffset":0,"strokeLineJoin":"miter","strokeUniform":false,"strokeMiterLimit":4,"scaleX":1,"scaleY":1,"angle":0,"flipX":false,"flipY":false,"opacity":1,"shadow":null,"visible":true,"backgroundColor":"","fillRule":"nonzero","paintFirst":"fill","globalCompositeOperation":"source-over","skewX":0,"skewY":0,"rx":0,"ry":0}]}'
    // canvas.loadFromJSON(JSON.parse(json), canvas.renderAll.bind(canvas));
    function checkOverlap(X,Y){
        let floatV = 5
        let x_index = -1;
        let y_index = -1;
        for(let x_i=1;x_i<=floatV;x_i++){
            if(allPointX.indexOf(X) > -1){
                x_index = allPointX.indexOf(X); 
                break;
            }
            if(allPointX.indexOf(X-x_i) > -1){
                x_index = allPointX.indexOf(X-x_i); 
                break;
            }
            if(allPointX.indexOf(X+x_i) > -1){
                x_index = allPointX.indexOf(X+x_i); 
                break;
            }
        } 
        let Y_flag = false;
        for(let y_i=1;y_i<=floatV;y_i++){
            if(allPointY.indexOf(Y) > -1){
                y_index = allPointX.indexOf(Y); 
                break;
            }
            if(allPointY.indexOf(Y-y_i) > -1){
                y_index = allPointX.indexOf(Y-y_i); 
                break;
            }
            if(allPointY.indexOf(Y+y_i) > -1){
                y_index = allPointX.indexOf(Y+y_i); 
                break;
            }
        }
        return [x_index>-1 && y_index>-1,allPointX[x_index],allPointY[y_index]];
    }
    
    canvas.on('mouse:move', function(options) {
        let res = checkOverlap(options.e.offsetX,options.e.offsetY)
        //校验是否该当前用户下棋
        if (isAllowedPlayChess){
            if(res[0]){
                $(".upper-canvas").removeClass("notAllowedCursor")
                $(".upper-canvas").removeClass("defaultCursor")
                $(".upper-canvas").addClass("pointerCursor")
            }else{
                $(".upper-canvas").removeClass("notAllowedCursor")
                $(".upper-canvas").removeClass("pointerCursor")
                $(".upper-canvas").addClass("defaultCursor")
            }
        }
    });

    canvas.on('mouse:down', function(options) {
        let res = checkOverlap(options.e.offsetX,options.e.offsetY)

        //校验是否该当前用户下棋
        if (isAllowedPlayChess){
            if(res[0]){
                //当前用户下完，将当前用户的【校验是否该当前用户下棋】设为false 鼠标设置为不可用
                isAllowedPlayChess = false
                $(".upper-canvas").removeClass("defaultCursor")
                $(".upper-canvas").removeClass("pointerCursor")
                $(".upper-canvas").addClass("notAllowedCursor")
                canvas.add(new fabric.Circle({
                    left : res[1]-9, //X
                    top : res[2]-9, //Y
                    radius : 10, //圆形半径
                    fill : whatSideColor, //填充的颜色
                    stroke: whatSideColor, //边框颜色
                    selectable:false
                }))
                // whatSideColor=="white"?whatSideColor="black":whatSideColor="white"
                syncPlayChessInfo(res[1],res[2])
            }
        }
    });
    
    //接收到消息的回调方法
    websocket.onmessage = function(event) {
            console.log(event)
            let message = JSON.parse(event.data)
            console.log(message)
            if(message.type == "MATCH"){
                whatSideColor = message.pieceColor
                //白棋先下
                if (whatSideColor == "white"){
                    isAllowedPlayChess = true
                }
                $("#battle-info").text("正在和【"+message.enemyUser+"】对局")
                $("#match-button").addClass("disabled")
                $("#match-input").addClass("disabled")
            }else if(message.type == "PLAY_CHESS"){
                console.log("PLAY_CHESS",message)
                canvas.add(new fabric.Circle({
                    left : message.playChessInfo.x-9, //X
                    top : message.playChessInfo.y-9, //Y
                    radius : 10, //圆形半径
                    fill : message.pieceColor, //填充的颜色
                    stroke: message.pieceColor, // 边框颜色
                    selectable:false
                }))
                //收到对方下棋通知后，将当前用户的下棋权限打开
                isAllowedPlayChess = true;
            }else if(message.type == "SUCCESS"){
                console.log("SUCCESS",message)
                //对局结束  下棋权限都关闭
                isAllowedPlayChess = false;
                canvas.add(new fabric.Circle({
                    left : message.playChessInfo.x-9, //X
                    top : message.playChessInfo.y-9, //Y
                    radius : 10, //圆形半径
                    fill : message.pieceColor, //填充的颜色
                    stroke: message.pieceColor, // 边框颜色
                    selectable:false
                }))
                $("#success").text(message.content)
                $('.ui.basic.modal')
                    .modal('show')
                ;
            }
    }

    for (let index = 1; index <= 19; index++) { 
            canvas.add( new fabric.Rect({
            top : index*30,
            left : 0,
            width : 600,
            height : 2,
            fill : 'black',
            selectable:false
        })); 
        canvas.add( new fabric.Rect({
            top : 0,
            left : index*30,
            width : 2,
            height : 600,
            fill : 'black', 
            selectable:false
        })); 
        allPointX[index] = index*30
        allPointY[index] = index*30 
    }

    //方法区========================================================
    function match(){
        let matchId = $("#battleID").val();
        if (matchId){
            message.whichUser = uuid;
            message.type = 'MATCH';
            message.enemyUser = matchId;
            websocket.send(JSON.stringify(message))
        }
    }
    function syncPlayChessInfo(x,y){ 
        message.type = 'PLAY_CHESS'; 
        message.playChessInfo.x = x
        message.playChessInfo.y = y
        websocket.send(JSON.stringify(message))
    }