package com.lmw.gobang.socket;

import com.alibaba.fastjson.JSON;
import com.lmw.gobang.data.BattleInfo;
import com.lmw.gobang.data.GamePlayerSocket;
import com.lmw.gobang.data.Message;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;


@Slf4j
@ServerEndpoint(value = "/goBang/{whichUser}")
@Component
public class GoBangSocket extends BattleInfo{


    /**
     * 连接建立成功调用的方法
     */
    @OnOpen
    public void onOpen(@PathParam("whichUser")String whichUser, Session session) {
        this.session = session;
        GamePlayerSocket.onlineCount.incrementAndGet(); // 在线数加1
        GamePlayerSocket.setGoBangAllSocket(whichUser,this); //用户socket信息放入缓存
        log.info("有新连接加入：{}，当前在线人数为：{}", whichUser, GamePlayerSocket.onlineCount.get());
    }

    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose(Session session) {
        GamePlayerSocket.onlineCount.decrementAndGet(); // 在线数减1
        log.info("有一连接关闭：{}，当前在线人数为：{}", session.getId(), GamePlayerSocket.onlineCount.get());
    }

    /**
     * 收到客户端消息后调用的方法
     *
     * @param messageStr 客户端发送过来的消息
     */
    @OnMessage
    public void onMessage(String messageStr, Session session) {
        System.out.println(messageStr);
        Message message = JSON.parseObject(messageStr, Message.class);
        if ("MATCH".equals(message.getType())){
            try {
                match(message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else if ("PLAY_CHESS".equals(message.getType())){
            try {
                syncPlayChessInfo(message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @OnError
    public void onError(Session session, Throwable error) {
        log.error("发生错误");
        error.printStackTrace();
    }

//    /**
//     * 服务端发送消息给客户端
//     */
//    private void sendMessage(String message, Session toSession) {
//        try {
//            log.info("服务端给客户端[{}]发送消息{}", toSession.getId(), message);
//            toSession.getBasicRemote().sendText(message);
//        } catch (Exception e) {
//            log.error("服务端发送消息给客户端失败：{}", e);
//        }
//    }
}