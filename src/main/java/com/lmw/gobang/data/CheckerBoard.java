package com.lmw.gobang.data;

import java.util.HashMap;

/**
 * 棋盘信息
 */
public class CheckerBoard {
    private Integer A = 30;
    private Integer HOW_LINE = 19;
    private String WHAT_SIDE;
    private HashMap<String,String> allPoint = new HashMap<>();

    public CheckerBoard(){
        //初始化 棋盘数据
        Integer[] X = new Integer[HOW_LINE];
        Integer[] Y = new Integer[HOW_LINE];
        for (int i = 1; i <= HOW_LINE; i++) {
            X[i-1] = i*A;
            Y[i-1] = i*A;
        }
        for (Integer x : X) {
            for (Integer y : Y) {
                allPoint.put(x+"-"+y,"");
            }
        }
    }


    public boolean checkWin(Integer x,Integer y,String whatSide){
        this.WHAT_SIDE = whatSide;
        allPoint.put(x+"-"+y,whatSide);
        boolean vertical = vertical(x, y);
        if (vertical){
            return vertical;
        }
        boolean level = level(x, y);
        if (level){
            return level;
        }
        boolean leftOblique = leftOblique(x, y);
        if (leftOblique){
            return leftOblique;
        }
        boolean rightOblique = rightOblique(x, y);
        if (rightOblique){
            return rightOblique;
        }
        return false;
    }




    /**
     * 计算 左斜方向是否有五子相连
     * @param x
     * @param y
     */
    private boolean leftOblique(Integer x,Integer y){
        //计算当前点的左上方 有几个连贯的当前点的颜色
        Integer leftTop = 0;
        for (int i = 1; i < 5; i++) {
            if (x-A*i == 0){
                break;
            }
            if (y-A*i == 0){
                break;
            }
            String winLine = x-A*i + "-" + (y-A*i);
            if (!WHAT_SIDE.equals(allPoint.get(winLine))){
                break;
            }
            leftTop = i;
        }
        if ( leftTop == 4 ){
            //当前下棋者胜利
            System.out.println("当前下棋者胜利");
            return true;
        }



        //计算当前点的右下方 有几个连贯的当前点的颜色
        Integer rightBottom = 0;
        for (int i = 1; i < 5; i++) {
            if (x+A*i==A*HOW_LINE){
                break;
            }
            if (y+A*i==A*HOW_LINE){
                break;
            }
            String winLine = x+A*i + "-" + (y+A*i);
            if (!WHAT_SIDE.equals(allPoint.get(winLine))){
                break;
            }
            rightBottom = i;
        }
        if ( rightBottom == 4 ){
            //当前下棋者胜利
            System.out.println("当前下棋者胜利");
            return true;
        }

        if (rightBottom + leftTop >= 4){
            //当前下棋者胜利
            System.out.println("当前下棋者胜利");
            return true;
        }
        return false;
    }

    /**
     * 计算 右斜方向是否有五子相连
     * @param x
     * @param y
     */
    private boolean rightOblique(Integer x,Integer y){
        //计算当前点的右上方 有几个连贯的当前点的颜色
        Integer rightTop = 0;
        for (int i = 1; i < 5; i++) {
            if (x-A*i==0){
                break;
            }
            if (y+A*i==A*HOW_LINE){
                break;
            }
            String winLine = x+A*i + "-" + (y-A*i);
            if (!WHAT_SIDE.equals(allPoint.get(winLine))){
                break;
            }
            rightTop = i;
        }
        if ( rightTop == 4 ){
            //当前下棋者胜利
            System.out.println("当前下棋者胜利");
            return true;
        }

        //计算当前点的左下方 有几个连贯的当前点的颜色
        Integer leftBottom = 0;
        for (int i = 1; i < 5; i++) {
            if (x-A*i == 0){
                break;
            }
            if (y+A*i == A*HOW_LINE){
                break;
            }
            String winLine = x-A*i + "-" + (y+A*i);
            if (!WHAT_SIDE.equals(allPoint.get(winLine))){
                break;
            }
            leftBottom = i;
        }
        if ( leftBottom == 4 ){
            //当前下棋者胜利
            System.out.println("当前下棋者胜利");
            return true;
        }

        if (leftBottom + rightTop >= 4){
            //当前下棋者胜利
            System.out.println("当前下棋者胜利");
            return true;
        }
        return false;
    }

    /**
     * 计算 水平方向是否有五子相连
     * @param x
     * @param y
     */
    private boolean level(Integer x,Integer y){
        //计算当前点的左方 有几个连贯的当前点的颜色
        Integer left = 0;
        for (int i = 1; i < 5; i++) {
            if (x-A*i==0){
                break;
            }
            String winLine = x-A*i + "-" + y;
            if (!WHAT_SIDE.equals(allPoint.get(winLine))){
                break;
            }
            left = i;
        }
        if ( left == 4 ){
            //当前下棋者胜利
            System.out.println("当前下棋者胜利");
            return true;
        }

        //计算当前点的右方 有几个连贯的当前点的颜色
        Integer right = 0;
        for (int i = 1; i < 5; i++) {
            if (x+A*i==A*HOW_LINE){
                break;
            }
            String winLine = x+A*i + "-" + y;
            if (!WHAT_SIDE.equals(allPoint.get(winLine))){
                break;
            }
            right = i;
        }
        if ( right == 4 ){
            //当前下棋者胜利
            System.out.println("当前下棋者胜利");
            return true;
        }

        if (right + left >= 4){
            //当前下棋者胜利
            System.out.println("当前下棋者胜利");
            return true;
        }
        return false;
    }

    /**
     * 计算 垂直方向是否有五子相连
     * @param x
     * @param y
     */
    private boolean vertical(Integer x,Integer y){
        //计算当前点的上方 有几个连贯的当前点的颜色
        Integer top = 0;
        for (int i = 1; i < 5; i++) {
            if (y-A*i==0){
                break;
            }
            String winLine = x + "-" +( y-A*i );
            if (!WHAT_SIDE.equals(allPoint.get(winLine))){
                break;
            }
            top = i;
        }
        if ( top == 4 ){
            //当前下棋者胜利
            System.out.println("当前下棋者胜利");
            return true;
        }

        //计算当前点的下方 有几个连贯的当前点的颜色
        Integer bottom = 0;
        for (int i = 1; i < 5; i++) {
            if (y+A*i==A*HOW_LINE){
                break;
            }
            String winLine = x + "-" +( y+A*i );
            if (!WHAT_SIDE.equals(allPoint.get(winLine))){
                break;
            }
            bottom = i;
        }
        if ( bottom == 4 ){
            //当前下棋者胜利
            System.out.println("当前下棋者胜利");
            return true;
        }

        if (bottom + top >= 4){
            //当前下棋者胜利
            System.out.println("当前下棋者胜利");
            return true;
        }
        return false;
    }
}
