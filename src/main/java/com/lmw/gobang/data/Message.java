package com.lmw.gobang.data;

import lombok.Getter;
import lombok.Setter;

/**
 * 封装每次通信的消息内容
 */

@Setter
@Getter
public class Message {
    /**
     * 下棋人
     */
    private String whichUser;
    /**
     * 消息类型 MATCH(匹配) PLAY_CHESS(下棋信息)
     */
    private String type;
    /**
     * 下棋点
     */
    private PlayChessInfo playChessInfo;

    /**
     * 要和谁对决
     */
    private String enemyUser;
}
