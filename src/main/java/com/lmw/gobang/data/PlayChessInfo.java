package com.lmw.gobang.data;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PlayChessInfo {
    private Integer x;
    private Integer y;
}
