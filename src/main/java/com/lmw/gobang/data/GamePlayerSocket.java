package com.lmw.gobang.data;

import com.lmw.gobang.socket.GoBangSocket;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 存放玩家的websocket信息
 */
public class GamePlayerSocket{
    //concurrent包的线程安全Map，用来存放每个客户端对应的MyWebSocket对象。
    private static ConcurrentHashMap<String, GoBangSocket> goBangAllSocket = new ConcurrentHashMap<>();

    //concurrent包的线程安全Map，用来存放每个客户端是否在游戏中。
    private static ConcurrentHashMap<String, String> goBangAllSocketStatus = new ConcurrentHashMap<>();
    //记录当前在线连接数
    public static AtomicInteger onlineCount = new AtomicInteger(0);

    public static void setGoBangAllSocketStatusIsBattle(String whichUser) {
        goBangAllSocketStatus.put(whichUser,"battle");
    }

    public static void setGoBangAllSocket(String whichUser, GoBangSocket goBangSocket){
        if (goBangAllSocket.contains(whichUser)){
            return;
        }
        goBangAllSocket.put(whichUser,goBangSocket);
    }

    public static GoBangSocket getGoBangSocket(String whichUser){
        return goBangAllSocket.get(whichUser);
    }
}
