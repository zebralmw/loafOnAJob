package com.lmw.gobang.data;

import com.alibaba.fastjson.JSON;
import com.lmw.gobang.socket.GoBangSocket;
import lombok.Getter;
import lombok.Setter;

import javax.websocket.Session;
import java.io.IOException;

/**
 * 存放当前两人的对战信息
 */
@Getter
@Setter
public class BattleInfo {
    //当前玩家的session会话
    public Session session;
    //哪一方
    private String whatSide;
    //对方的socket通道
    private GoBangSocket goBangSocket;
    //存放当前对局的棋盘信息
    private CheckerBoard checkerBoard;



    public void match(Message message) throws IOException {
        String enemyUser = message.getEnemyUser();
        String whichUser = message.getWhichUser();
        GoBangSocket black = GamePlayerSocket.getGoBangSocket(enemyUser);
        GoBangSocket white = GamePlayerSocket.getGoBangSocket(whichUser);
        CheckerBoard checkerBoard = new CheckerBoard();
        //设置对手
        this.goBangSocket = black;
        this.whatSide = "white";
        this.checkerBoard = checkerBoard;
        ResponseMessage whiteResponseMessage = new ResponseMessage();
        whiteResponseMessage.setType("MATCH");
        whiteResponseMessage.setContent("匹配成功");
        whiteResponseMessage.setPieceColor("white");
        whiteResponseMessage.setEnemyUser(enemyUser);
        white.session.getBasicRemote().sendText(JSON.toJSONString(whiteResponseMessage));
        this.goBangSocket.setGoBangSocket(white);
        this.goBangSocket.setWhatSide("black");
        this.goBangSocket.setCheckerBoard(checkerBoard);

        ResponseMessage blackResponseMessage = new ResponseMessage();
        blackResponseMessage.setType("MATCH");
        blackResponseMessage.setContent("匹配成功");
        blackResponseMessage.setPieceColor("black");
        blackResponseMessage.setEnemyUser(whichUser);
        black.session.getBasicRemote().sendText(JSON.toJSONString(blackResponseMessage));
        System.out.println("匹配成功");
    }

    public void syncPlayChessInfo(Message message) throws IOException {
        PlayChessInfo playChessInfo = message.getPlayChessInfo();
        ResponseMessage responseMessage = new ResponseMessage();
        responseMessage.setPlayChessInfo(playChessInfo);
        responseMessage.setType("PLAY_CHESS");
        responseMessage.setPieceColor(whatSide);
        boolean checkWin = this.checkerBoard.checkWin(playChessInfo.getX(), playChessInfo.getY(), whatSide);
        System.out.println(checkWin);
        //当前玩家获胜 通知双方
        if (checkWin){
            responseMessage.setType("SUCCESS");
            responseMessage.setContent(whatSide+"方获胜");
            this.session.getBasicRemote().sendText(JSON.toJSONString(responseMessage));
        }
        this.goBangSocket.session.getBasicRemote().sendText(JSON.toJSONString(responseMessage));
    }
}
