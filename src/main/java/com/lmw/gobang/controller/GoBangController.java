package com.lmw.gobang.controller;
import cn.hutool.core.util.RandomUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(("goBang"))
public class GoBangController {
    //首页
    @GetMapping("index")
    public String index(Model model) {
        List<Map<String,String>> list = new ArrayList<>();
        //模拟用户数据
        for (int i = 0; i < 20; i++) {
            Map<String,String> user = new HashMap<>();
            user.put("name", RandomUtil.randomString(8));
            list.add(user);
        }
        model.addAttribute("userList",list);
        return "index";
    }
    //棋盘界面
    @GetMapping("checkerBoard")
    public String checkerBoard(Model model) {
        String s = RandomUtil.randomString(10);
        model.addAttribute("ID",s);
        return "checkerBoard";
    }








}
