package com.lmw.utils;

import cn.hutool.cache.CacheUtil;
import cn.hutool.cache.impl.TimedCache;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.RandomUtil;
import com.lmw.youDrawIGuess.entity.DrawUser;
import com.lmw.youDrawIGuess.entity.Thesaurus;
import com.lmw.youDrawIGuess.entity.User;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class LoafOnAJobCacheUtil {
    //验证码缓存
    static TimedCache<String, String> CODE_CACHE = CacheUtil.newTimedCache(1000*60*10);

    //你画我猜用户缓存
    static TimedCache<String, User> USER_CACHE = CacheUtil.newTimedCache(1000*60*30);

    //你画我猜词库
    static List<String> DRAW_THESAURUS;

    public static void initDrawThesaurus(List<String> DRAW_THESAURUS){
        LoafOnAJobCacheUtil.DRAW_THESAURUS = DRAW_THESAURUS;
    }

    /**
     * 随机获取6个词汇供选择
     * @return
     */
    public static Set<String> getRandomDrawThesaurus(){
        Integer size = 6;
        Set<String> set = new HashSet<>(size);
        while (set.size()<size){
            int index = RandomUtil.randomInt(0, DRAW_THESAURUS.size());
            set.add(DRAW_THESAURUS.get(index));
        }
        return set;
    }

    public static User getUserCache(String userId) {
        return USER_CACHE.get(userId);
    }

    public static void setUserCache(User user) {
        USER_CACHE.put(user.getUserId(),user);
    }

    public static boolean checkCode(String email, String code){
        String cacheCode = CODE_CACHE.get(email);
        if (ObjectUtil.isNull(cacheCode) || ObjectUtil.isNull(code)){
            return false;
        }
        return cacheCode.equals(code);
    }

    public static void setCodeCache(String email,String code){
        CODE_CACHE.put(email,code);
    }


    public static void main(String[] args) {

        Set<Integer> set = new HashSet<>(6);
        System.out.println(set.size());
        int i = RandomUtil.randomInt(0, 97);
    }

}
