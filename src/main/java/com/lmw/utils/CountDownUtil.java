package com.lmw.utils;

import cn.hutool.core.lang.Console;
import cn.hutool.cron.CronUtil;
import cn.hutool.cron.task.Task;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author liumengwei
 * @Description TODO
 * @Date 2022/7/5 16:21
 * @Version 1.0
 */
public class CountDownUtil {
    public volatile static Long COUNT = 0l;

    public volatile static Map<String,String> COUNT_DOWN_LIST = new ConcurrentHashMap<>();


    public static void addCountDown(){

    }

    public static void initCountDownSchedule(){
        CronUtil.schedule("*/1 * * * * *", new Task() {
            @Override
            public void execute() {
                COUNT++;
                Console.log("Task excuted.");
            }
        });
        CronUtil.setMatchSecond(true);
        CronUtil.start();
    }
}
