package com.lmw.utils;

import com.alibaba.druid.filter.config.ConfigTools;

/**
 * @Author liumengwei
 * @Description TODO
 * @Date 2021/9/16 14:23
 * @Version 1.0
 */
public class DruidPassword {
    public static void main(String[] args) throws Exception {
        String password = "root@lmw@1024";
        String[] arr = ConfigTools.genKeyPair(512);
        System.out.println("privateKey:" + arr[0]);
        System.out.println("publicKey:" + arr[1]);
        System.out.println("password:" + ConfigTools.encrypt(arr[0], password));
    }
}
