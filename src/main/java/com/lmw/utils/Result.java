package com.lmw.utils;

import java.io.Serializable;

public class Result<T> implements Serializable {
       private static final Integer SUCCESS = 200;

       private static final Integer FAIL = 500;

       private Integer code;

       private String msg;

       private T Data;

       public Result(Integer code)
       {
        this.code = code;
       }

       public Result(Integer code, String msg)
       {
        super();
        this.code = code;
        this.msg = msg;
       }

       public Result(Integer code, String msg, T data)
       {
        super();
        this.code = code;
        this.msg = msg;
        Data = data;
       }

       public Integer getCode()
       {
        return code;
       }

       public void setCode(Integer code)
       {
        this.code = code;
       }

       public String getMsg()
       {
        return msg;
       }

       public void setMsg(String msg)
       {
        this.msg = msg;
       }

       public T getData()
       {
        return Data;
       }

       public void setData(T data)
       {
        Data = data;
       }

       public static <T> Result<T> ok(T object)
       {
        return new Result<T>(SUCCESS, "", object);
       }

       public static <T> Result<T> ok()
       {
        return new Result<T>(SUCCESS);
       }

       public static <T> Result<T> nok(String msg)
       {
        return new Result<T>(FAIL, msg);
       }

       public static <T> Result<T> nok()
       {
        return new Result<T>(FAIL);
       }

       public static <T> Result<T> nok(Integer code, String msg)
       {
        return new Result<T>(code, msg);
       }

       public static <T> Result<T> nok(Integer code, String msg, T object)
       {
        return new Result<T>(code, msg, object);
       }

}