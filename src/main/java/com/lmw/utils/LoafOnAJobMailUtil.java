package com.lmw.utils;

import cn.hutool.cache.CacheUtil;
import cn.hutool.cache.impl.TimedCache;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.text.StrFormatter;
import cn.hutool.core.util.ReUtil;
import cn.hutool.extra.mail.MailAccount;
import cn.hutool.extra.mail.MailUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * @Author liumengwei
 * @Description TODO
 * @Date 2022/6/17 14:45
 * @Version 1.0
 */
@Slf4j
public class LoafOnAJobMailUtil {
    private static final String EMAIL_REG = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";


    private static MailAccount account;
    static {
        account = new MailAccount();
        account.setHost("smtp.qq.com");
        account.setPort(465);
        account.setAuth(true);
        account.setFrom("251282149@qq.com");
        account.setPass("nokzhuutbvpfbhca");
        account.setSocketFactoryClass("javax.net.ssl.SSLSocketFactory");
        account.setSocketFactoryPort(465);
        account.setSslEnable(true);
        account.setSocketFactoryFallback(false);
        account.isStarttlsEnable();
    }


    public static void main(String[] args) {
        sendCaptcha("18810092585@163.com","test");
    }
    public static Result sendCaptcha(String toEmail,String code){
        log.info("邮箱:{},验证码:{}",toEmail,code);
        if (!ReUtil.isMatch(EMAIL_REG,toEmail)){
            log.info("邮箱不正确");
            return Result.nok("邮箱不正确");
        }
        String send = MailUtil.send(account, CollUtil.newArrayList(toEmail), "注册摸鱼", StrFormatter.format("验证码【{}】十分钟有效",code), false);
        log.info("发送验证码结果:{}",send);
        return Result.ok("发送成功");
    }
}
