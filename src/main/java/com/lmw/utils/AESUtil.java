package com.lmw.utils;

import cn.hutool.crypto.Mode;
import cn.hutool.crypto.Padding;
import cn.hutool.crypto.symmetric.AES;


/**
 * @Author liumengwei
 * @Description TODO
 * @Date 2021/12/9 16:11
 * @Version 1.0
 */
public class AESUtil {
    private static AES aes = null;
    static {
        try {
            aes = new AES(Mode.CBC, Padding.ZeroPadding, "qwertyuiopasdfgh".getBytes("UTF-8"));
            aes.setIv("qwertyuiopasdfgh".getBytes("UTF-8"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String decrypt(String e){
        return aes.decryptStr(e);
    }
}
