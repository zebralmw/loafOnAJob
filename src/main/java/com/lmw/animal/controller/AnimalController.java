package com.lmw.animal.controller;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.lmw.animal.data.GamePlayerSocket;
import com.lmw.animal.socket.AnimalSocket;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Controller
@RequestMapping(("animal"))
public class AnimalController {
    //棋盘界面
    @GetMapping
    public String animal(Model model) {
        String s = RandomUtil.randomString(10);
        model.addAttribute("ID",s);
        return "animal";
    }

    @GetMapping("onlineUser")
    @ResponseBody
    public List<String> onlineUser() {
        List<String> strings = new ArrayList<>();
        ConcurrentHashMap<String, AnimalSocket> animalAllSocket = GamePlayerSocket.getAnimalAllSocket();
        ConcurrentHashMap.KeySetView<String, AnimalSocket> strings1 = animalAllSocket.keySet();
        for (String s : strings1) {
            AnimalSocket animalSocket = animalAllSocket.get(s);
            if (animalSocket.getAnimalSocket() == null){
                strings.add(s);
            }
        }
        return strings;
    }
}
