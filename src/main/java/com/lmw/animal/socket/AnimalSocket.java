package com.lmw.animal.socket;

import com.alibaba.fastjson.JSON;
import com.lmw.animal.data.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;


@Slf4j
@ServerEndpoint(value = "/animal/{whichUser}")
@Component
public class AnimalSocket extends BattleInfo {


    /**
     * 连接建立成功调用的方法
     */
    @OnOpen
    public void onOpen(@PathParam("whichUser")String whichUser, Session session) {
        this.session = session;
        GamePlayerSocket.setAnimalAllSocket(whichUser,this); //用户socket信息放入缓存
        log.info("有新连接加入：{}", whichUser);
    }

    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose(Session session) {
        GamePlayerSocket.removeSocket(session.getId());
        log.info("有一连接关闭：{}，当前在线人数为：{}", session.getId());
    }

    /**
     * 收到客户端消息后调用的方法
     *
     * @param messageStr 客户端发送过来的消息
     */
    @OnMessage
    public void onMessage(String messageStr, Session session) {
        System.out.println(messageStr);
        ClientMessage message = JSON.parseObject(messageStr, ClientMessage.class);

        if ("MATCH".equals(message.getType())){
            try {
                match(message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else if ("PLAY".equals(message.getType())){
            try {
                play(message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else if ("CHAT".equals(message.getType())){
            try {
                chat(message);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    @OnError
    public void onError(Session session, Throwable error) {
        log.error("发生错误");
        error.printStackTrace();
    }


}