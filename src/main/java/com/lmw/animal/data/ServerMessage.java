package com.lmw.animal.data;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author liumengwei
 * @Description TODO
 * @Date 2021/9/10 15:05
 * @Version 1.0
 */
@Getter
@Setter
public class ServerMessage {
    //响应类型
    private String responseType;
    //每次响应都要携带最新的棋盘信息
    private CheckerBoard checkerBoard;
    private String pieceColor;
    private Integer responseCode;
    private String otherParty;
    private String allowMovePieceColor;
    private String content;
    private ChatBean chat;
}
