package com.lmw.animal.data;

import com.alibaba.fastjson.JSON;

import javax.websocket.Session;
import java.io.IOException;

/**
 * @Author liumengwei
 * @Description TODO
 * @Date 2021/9/13 9:47
 * @Version 1.0
 */
public class SocketResponse {

    public static void inGame(Session session){
        ServerMessage serverMessage = new ServerMessage();
        serverMessage.setResponseType("ERROR");
        serverMessage.setContent("该玩家正在游戏中");
        try {
            session.getBasicRemote().sendText(JSON.toJSONString(serverMessage));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void noUser(Session session){
        ServerMessage serverMessage = new ServerMessage();
        serverMessage.setResponseType("ERROR");
        serverMessage.setContent("不存在该用户");
        try {
            session.getBasicRemote().sendText(JSON.toJSONString(serverMessage));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void notMatchSelf(Session session){
        ServerMessage serverMessage = new ServerMessage();
        serverMessage.setResponseType("ERROR");
        serverMessage.setContent("不能匹配自己");
        try {
            session.getBasicRemote().sendText(JSON.toJSONString(serverMessage));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
