package com.lmw.animal.data;

import com.alibaba.fastjson.JSON;

import java.util.*;

/**
 * @Author liumengwei
 * @Description TODO
 * @Date 2021/9/10 14:46
 * @Version 1.0
 */
public class AnimalUtil {
    static String[] arr = {"blue-象","blue-狮","blue-虎","blue-豹","blue-狼","blue-狗","blue-猫","blue-鼠","red-象","red-狮","red-虎","red-豹","red-狼","red-狗","red-猫","red-鼠"};

    public static Map initPiece(){
        Map result = new HashMap<>();
        //点映射棋子
        Map pointMappingPiece = new HashMap<>();
        //棋子映射点
        Map pieceMappingPoint = new HashMap<>();

        List<String> strings = new ArrayList<>(Arrays.asList(arr));
        Collections.shuffle(strings);
        for (int i = 0; i < strings.size(); i++) {
            pointMappingPiece.put(i+1,strings.get(i));
            pieceMappingPoint.put(strings.get(i),i+1);
        }
        result.put("pointMappingPiece",pointMappingPiece);
        result.put("pieceMappingPoint",pieceMappingPoint);
        return result;
    }


    public static void main(String[] args) {
        Map map = initPiece();

        System.out.println(JSON.toJSONString(map));
    }
}
