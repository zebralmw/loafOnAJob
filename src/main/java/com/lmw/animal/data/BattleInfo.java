package com.lmw.animal.data;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.lmw.animal.socket.AnimalSocket;
import com.lmw.gobang.data.*;
import com.lmw.animal.socket.AnimalSocket;
import lombok.Getter;
import lombok.Setter;

import javax.websocket.Session;
import java.io.IOException;
import java.util.Date;

/**
 * 存放当前两人的对战信息
 */
@Getter
@Setter
public class BattleInfo{
    //当前玩家的session会话
    public Session session;
    //哪一方
    private String whatSide;
    //对方的socket通道
    private AnimalSocket animalSocket;
    //存放当前对局的棋盘信息
    private CheckerBoard checkerBoard;



    public void match(ClientMessage message) throws IOException {
        String redUser = message.getRedUser();
        String blueUser = message.getBlueUser();
        AnimalSocket redSocket = GamePlayerSocket.getAnimalSocket(redUser);
        AnimalSocket blueSocket = GamePlayerSocket.getAnimalSocket(blueUser);
        //不存在用户
        if (StrUtil.isEmpty(redUser) || StrUtil.isEmpty(blueUser) || redSocket == null || blueSocket == null){
            SocketResponse.noUser(session);
            return;
        }
        redUser = redUser.trim();
        blueUser = blueUser.trim();
        //不能匹配自己
        if (redUser.equals(blueUser)){
            SocketResponse.notMatchSelf(session);
            return;
        }
        //判断匹配的对手是否在游戏中
        if (blueSocket.getAnimalSocket()!=null){
            SocketResponse.inGame(session);
            return;
        }
        CheckerBoard checkerBoard = new CheckerBoard();
        redSocket.setCheckerBoard(checkerBoard);
        redSocket.setAnimalSocket(blueSocket);
        redSocket.setWhatSide("red");
        blueSocket.setCheckerBoard(checkerBoard);
        blueSocket.setAnimalSocket(redSocket);
        blueSocket.setWhatSide("blue");
        ServerMessage redServerMessage = new ServerMessage();
        redServerMessage.setCheckerBoard(checkerBoard);
        redServerMessage.setResponseType("INIT");
        redServerMessage.setPieceColor("red");
        redServerMessage.setOtherParty(blueUser);
        redServerMessage.setAllowMovePieceColor("red");
        redSocket.getSession().getBasicRemote().sendText(JSON.toJSONString(redServerMessage));

        ServerMessage blueServerMessage = new ServerMessage();
        blueServerMessage.setCheckerBoard(checkerBoard);
        blueServerMessage.setResponseType("INIT");
        blueServerMessage.setPieceColor("blue");
        blueServerMessage.setOtherParty(redUser);
        blueServerMessage.setAllowMovePieceColor("red");
        blueSocket.getSession().getBasicRemote().sendText(JSON.toJSONString(blueServerMessage));
    }

    public void play(ClientMessage message) throws IOException {
        if ("OPEN".equals(message.getPlayType())){
            ServerMessage serverMessage = new ServerMessage();
            serverMessage.setResponseType("RESET");
            AnimalSocket playSocket = GamePlayerSocket.getAnimalSocket(message.getPlayUser());
            if (playSocket.getWhatSide().equals("red")){
                serverMessage.setAllowMovePieceColor("blue");
            }else{
                serverMessage.setAllowMovePieceColor("red");
            }
            CheckerBoard checkerBoard = playSocket.getCheckerBoard();
            checkerBoard.openPiece(message.getOpenPieceIndex());
            serverMessage.setCheckerBoard(checkerBoard);
            playSocket.getSession().getBasicRemote().sendText(JSON.toJSONString(serverMessage));
            playSocket.getAnimalSocket().getSession().getBasicRemote().sendText(JSON.toJSONString(serverMessage));
        } else if ("MOVE".equals(message.getPlayType())){
            AnimalSocket playSocket = GamePlayerSocket.getAnimalSocket(message.getPlayUser());
            CheckerBoard checkerBoard = playSocket.getCheckerBoard();
            Integer move = checkerBoard.move(message.getOriginalPosition(), message.getPositionToMove(),whatSide);
            ServerMessage serverMessage = new ServerMessage();
            serverMessage.setResponseCode(move);
            serverMessage.setCheckerBoard(checkerBoard);
            if (move > -1){
                serverMessage.setResponseType("RESET");
                if (playSocket.getWhatSide().equals("red")){
                    serverMessage.setAllowMovePieceColor("blue");
                }else{
                    serverMessage.setAllowMovePieceColor("red");
                }
                playSocket.getSession().getBasicRemote().sendText(JSON.toJSONString(serverMessage));
                playSocket.getAnimalSocket().getSession().getBasicRemote().sendText(JSON.toJSONString(serverMessage));
            }else{ //只提示自己
                serverMessage.setResponseType("NO-CHANGE");
                playSocket.getSession().getBasicRemote().sendText(JSON.toJSONString(serverMessage));
            }
        }
    }
    public void chat(ClientMessage message) throws IOException {
        AnimalSocket playSocket = GamePlayerSocket.getAnimalSocket(message.getPlayUser());
        ServerMessage serverMessage = new ServerMessage();
        serverMessage.setResponseType("CHAT");
        ChatBean self = new ChatBean();
        self.setContent(message.getContent());
        self.setUser("自己");
        self.setColor(playSocket.getWhatSide());
        self.setTime(DateUtil.formatDateTime(new Date()));
        serverMessage.setChat(self);
        playSocket.getSession().getBasicRemote().sendText(JSON.toJSONString(serverMessage));
        AnimalSocket animalSocket = playSocket.getAnimalSocket();
        ChatBean others = new ChatBean();
        others.setContent(message.getContent());
        others.setUser("对方");
        others.setColor(playSocket.getWhatSide());
        others.setTime(DateUtil.formatDateTime(new Date()));
        serverMessage.setChat(others);
        animalSocket.getSession().getBasicRemote().sendText(JSON.toJSONString(serverMessage));
    }

    public static void main(String[] args) {
        System.out.println(DateUtil.formatDateTime(new Date()));
    }
}
