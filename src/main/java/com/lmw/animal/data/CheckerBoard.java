package com.lmw.animal.data;

import lombok.Getter;
import lombok.Setter;

import java.util.*;

/**
 * 棋盘信息
 */
@Getter
@Setter
public class CheckerBoard {
    static String[] arr = {"blue-象-7","blue-狮-6","blue-虎-5","blue-豹-4","blue-狼-3","blue-狗-2","blue-猫-1","blue-鼠-0","red-象-7","red-狮-6","red-虎-5","red-豹-4","red-狼-3","red-狗-2","red-猫-1","red-鼠-0"};
    //点映射棋子
    private Map<String,AnimalPiece> pointMappingPiece = new HashMap<>();

    private Integer[][] boardPoint = null;
//    //棋子映射点
//    private Map<String,String> pieceMappingPoint = new HashMap<>();

    public CheckerBoard(){
        boardPoint = initBoardPoint();

        List<AnimalPiece> animalPieces = new ArrayList<AnimalPiece>();
        for (String s : arr) {
            String[] split = s.split("-");
            AnimalPiece animalPiece = new AnimalPiece(split[0],split[1],Integer.valueOf(split[2]));
            animalPieces.add(animalPiece);
        }
        Collections.shuffle(animalPieces);
        for (int i = 0; i < animalPieces.size(); i++) {
            AnimalPiece animalPiece = animalPieces.get(i);
            animalPiece.setLeft(boardPoint[i+1][0]);
            animalPiece.setTop(boardPoint[i+1][1]);
            pointMappingPiece.put(String.valueOf(i+1),animalPiece);
        }
        pointMappingPiece.put(String.valueOf(0),null);

//        List<String> strings = new ArrayList<>(Arrays.asList(arr));
//        Collections.shuffle(strings);
//        for (int i = 0; i < strings.size(); i++) {
//            pointMappingPiece.put(i+1+"",strings.get(i));
//            pieceMappingPoint.put(strings.get(i),i+1+"");
//        }
    }

    public void openPiece(String pointKey){
        AnimalPiece animalPiece = pointMappingPiece.get(pointKey);
        animalPiece.setOpen(true);
        pointMappingPiece.put(pointKey,animalPiece);
    }

    /**
     *  -1 棋子小于对方  -2 两种相同颜色不能移动 -3移动棋子位置未翻开
     *  -4不能移动到当前位置  -5 不能移动对方的棋子 -6 不能移动未翻开的棋子
     *  -7中心点已有棋子
     * @param originalPosition
     * @param positionToMove
     * @return
     */
    public Integer move(String originalPosition,String positionToMove,String whatSide){
        AnimalPiece originalPiece = pointMappingPiece.get(originalPosition); //猫

        if (!originalPiece.isOpen()){
            return -6;
        }
        if (!originalPiece.checkColor(whatSide)){
            return -5;
        }
        boolean b = computerAllowedMove(originalPosition, positionToMove);
        if (!b){
            return -4;
        }
        AnimalPiece positionPiece = pointMappingPiece.get(positionToMove); //狼
        if (positionPiece == null){
            originalPiece.setLeft(boardPoint[Integer.valueOf(positionToMove)][0]);
            originalPiece.setTop(boardPoint[Integer.valueOf(positionToMove)][1]);
            pointMappingPiece.put(positionToMove,originalPiece);
            pointMappingPiece.put(originalPosition,null);
            return 1;
        }else{
            if (Integer.valueOf(positionToMove) == 0){
                return -7;
            }
            Integer compareTo = originalPiece.compareTo(positionPiece);
             if(compareTo==0){
                pointMappingPiece.put(positionToMove,null);
                pointMappingPiece.put(originalPosition,null);
            }else if(compareTo==1){
                originalPiece.setLeft(boardPoint[Integer.valueOf(positionToMove)][0]);
                originalPiece.setTop(boardPoint[Integer.valueOf(positionToMove)][1]);
                pointMappingPiece.put(positionToMove,originalPiece);
                pointMappingPiece.put(originalPosition,null);
            }
             return compareTo;
        }
    }



    private boolean computerAllowedMove(String originalPosition,String positionToMove){
        Integer o = Integer.valueOf(originalPosition);
        Integer p = Integer.valueOf(positionToMove);
        List<Integer> list = allowedMove(o);

        if (list.contains(p)){
            return true;
        }
        return false;

    }

    private List<Integer> allowedMove(Integer pointKey){
        List<Integer> result = new ArrayList<>();
        List<Integer> centerPoint = new ArrayList<Integer>(){{
            add(6);
            add(7);
            add(10);
            add(11);
        }};
        if (pointKey==0){
            return centerPoint;
        }
        if (centerPoint.contains(pointKey)){
            result.add(0);
        }
        if(pointKey%4==2 || pointKey%4==3){
            result.add(pointKey-1);
            result.add(pointKey+1);
            if(pointKey-4>0){
                result.add(pointKey-4);
            }
            if(pointKey+4<=16){
                result.add(pointKey+4);
            }
        }else if(pointKey%4==0){
            result.add(pointKey-1);
            if(pointKey-4>0){
                result.add(pointKey-4);
            }
            if(pointKey+4<=16){
                result.add(pointKey+4);
            }
        }else if(pointKey%4==1){
            result.add(pointKey+1);
            if(pointKey-4>0){
                result.add(pointKey-4);
            }
            if(pointKey+4<=16){
                result.add(pointKey+4);
            }
        }
        return result;
    }
    /**
     * 初始化棋盘的各个点坐标  arr[0] = x坐标即left arr[1] = y坐标即top
     * @return
     */
    private Integer[][] initBoardPoint(){
        Integer[][] result = new Integer[17][2];
        Integer[] x = new Integer[4];
        Integer[] y = new Integer[4];
        for (int i = 0; i <= 3; i++) {
            x[i] =  i * 100 + 50;
            y[i] =  i * 100 + 50;
        }
        //中间×的坐标默认初始化
        result[0] = new Integer[]{180,180};
        Integer i = 1;
        for (Integer integerx : x) {
            for (Integer integery : y) {
                result[i] = new Integer[]{integery-20,integerx-20};
                i++;
            }
        }
        return result;
    }
}
