package com.lmw.animal.data;

import com.lmw.animal.socket.AnimalSocket;
import com.lmw.gobang.socket.GoBangSocket;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 存放玩家的websocket信息
 */
public class GamePlayerSocket {
    //concurrent包的线程安全Map，用来存放每个客户端对应的MyWebSocket对象。
    private static ConcurrentHashMap<String, AnimalSocket> animalAllSocket = new ConcurrentHashMap<>();
    private static ConcurrentHashMap<String, String> userIdMappingSessionId = new ConcurrentHashMap<>();


    public static void removeSocket(String sessionId){
        String s = userIdMappingSessionId.get(sessionId);
        animalAllSocket.remove(s);
        userIdMappingSessionId.remove(sessionId);
    }


    public static void setAnimalAllSocket(String whichUser, AnimalSocket animalSocket){
        if (animalAllSocket.contains(whichUser)){
            return;
        }
        userIdMappingSessionId.put(animalSocket.getSession().getId(),whichUser);
        animalAllSocket.put(whichUser,animalSocket);
    }

    public static AnimalSocket getAnimalSocket(String whichUser){
        return animalAllSocket.get(whichUser);
    }

    public static ConcurrentHashMap<String, AnimalSocket> getAnimalAllSocket(){
        return animalAllSocket;
    }
}
