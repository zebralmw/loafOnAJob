package com.lmw.animal.data;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author liumengwei
 * @Description TODO
 * @Date 2021/9/13 10:18
 * @Version 1.0
 */
@Getter
@Setter
public class ChatBean {
    private String user;
    private String content;
    private String time;
    private String color;
}
