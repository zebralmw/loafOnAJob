package com.lmw.animal.data;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AnimalPiece {
    private String keyName;
    private String pieceColor;
    private String pieceName;
    private Integer level;
    private Integer left;
    private Integer top;
    private boolean open;
    public AnimalPiece(String pieceColor,String pieceName,Integer level){
        this.pieceColor = pieceColor;
        this.pieceName = pieceName;
        this.level = level;
    }
    public AnimalPiece(){

    }
    public String getKeyName(){
        return pieceColor+"-"+pieceName;
    }


    public boolean checkColor(String whatSide){
        return this.pieceColor.equals(whatSide);
    }
    /**
     * 象>狮>虎>豹>狼>狗>猫>鼠
     * 鼠只能吃大象
     * -1 棋子小于对方  -2 两种相同颜色不能移动 -3移动棋子位置未翻开 -3不能移动未翻开的棋子
     * @return
     */
    public int compareTo(AnimalPiece positionPiece) {
        AnimalPiece originalPiece = this; //猫
        if (originalPiece.getPieceColor().equals(positionPiece.getPieceColor())){
            return -2;
        }
        if (!positionPiece.isOpen()){
            return -3;
        }
        if (originalPiece.getLevel() == 0 && positionPiece.getLevel() == 7){
            return 1;
        }
        if (originalPiece.getLevel() == 7 && positionPiece.getLevel() == 0){
            return -1;
        }
        if (originalPiece.getLevel() == positionPiece.getLevel()){
            return 0;
        }else if (originalPiece.getLevel() > positionPiece.getLevel()){
            return 1;
        }else{
            return -1;
        }
    }
}
