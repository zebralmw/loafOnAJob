package com.lmw.animal.data;

import cn.hutool.core.util.StrUtil;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author liumengwei
 * @Description TODO
 * @Date 2021/9/10 15:04
 * @Version 1.0
 */
@Setter
@Getter
public class ClientMessage {
    private String type;
    private String playType;
    private String openPieceIndex;
    private String originalPosition;
    private String positionToMove;
    private String playUser;
    /**
     * 红方
     */
    private String redUser;
    /**
     * 蓝方
     */
    private String blueUser;

    private String content;

    public void setPlayUser(String playUser) {
        if (StrUtil.isNotEmpty(playUser)){
            playUser = playUser.trim();
        }
        this.playUser = playUser;
    }

    public void setRedUser(String redUser) {
        if (StrUtil.isNotEmpty(redUser)){
            redUser = redUser.trim();
        }
        this.redUser = redUser;
    }

    public void setBlueUser(String blueUser) {
        if (StrUtil.isNotEmpty(blueUser)){
            blueUser = blueUser.trim();
        }
        this.blueUser = blueUser;
    }
}
