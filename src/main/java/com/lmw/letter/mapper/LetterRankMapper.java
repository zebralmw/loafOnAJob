package com.lmw.letter.mapper;

import com.lmw.letter.bean.LetterRank;

import java.util.List;

/**
 * @Author liumengwei
 * @Description TODO
 * @Date 2021/12/9 10:30
 * @Version 1.0
 */
public interface LetterRankMapper {
    List<LetterRank> getList();

    List<LetterRank> getRanks();

    Integer addRank(LetterRank letterRank);
}
