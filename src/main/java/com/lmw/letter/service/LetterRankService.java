package com.lmw.letter.service;

import com.lmw.letter.bean.LetterRank;

import java.util.List;

/**
 * @Author liumengwei
 * @Description TODO
 * @Date 2021/12/9 10:25
 * @Version 1.0
 */
public interface LetterRankService {
    List<LetterRank> getList();

    List<LetterRank> getRanks();

    Integer addRank(String letterRank);
}
