package com.lmw.letter.service.impl;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.lmw.letter.bean.LetterRank;
import com.lmw.letter.mapper.LetterRankMapper;
import com.lmw.letter.service.LetterRankService;
import com.lmw.utils.AESUtil;
import com.lmw.utils.HttpRequestUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * @Author liumengwei
 * @Description TODO
 * @Date 2021/12/9 10:25
 * @Version 1.0
 */
@Service
@Slf4j
public class LetterRankServiceImpl implements LetterRankService {

//    @Resource
    private LetterRankMapper letterRankMapper;


    @Resource
    private HttpServletRequest request;
    @Override
    public List<LetterRank> getList() {
        return letterRankMapper.getList();
    }

    @Override
    public List<LetterRank> getRanks() {
        return letterRankMapper.getRanks();
    }

    @Override
    public Integer addRank(String letterRank) {
        try {
            String decrypt = AESUtil.decrypt(letterRank);
            JSONObject jsonObject = JSONObject.parseObject(decrypt);
            LetterRank rank = new LetterRank();
            if (StrUtil.isEmpty(jsonObject.getString("userName"))){
                rank.setUserName(HttpRequestUtil.getRealIp(request));
            }else{
                rank.setUserName(jsonObject.getString("userName"));
            }
            rank.setUserScore(jsonObject.getInteger("userScore"));
            rank.setPlayTime(jsonObject.getInteger("playTime"));
            rank.setCreateTime(new Date());
            return letterRankMapper.addRank(rank);
        }catch (Exception e) {
            log.error("解密失败", e);
            return 0;
        }
    }

}
