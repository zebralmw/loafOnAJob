package com.lmw.letter.controller;

import com.alibaba.fastjson.JSON;
import com.lmw.letter.bean.LetterRank;
import com.lmw.letter.service.LetterRankService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping(("letter"))
public class LetterController {
    @Resource
    private LetterRankService letterRankService;


    @GetMapping
    public String letter() {
        return "letter/index";
    }

    @PostMapping("addRank")
    @ResponseBody
    public Integer addRank(String letterRank) {
        System.out.println(letterRank);
        return letterRankService.addRank(letterRank);
    }

    @GetMapping("getRanks")
    @ResponseBody
    public Object getRanks() {
        List<LetterRank> ranks = letterRankService.getRanks();
        return ranks;
    }


}
