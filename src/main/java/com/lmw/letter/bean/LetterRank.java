package com.lmw.letter.bean;

import java.util.Date;

/**
 * @Author liumengwei
 * @Description TODO
 * @Date 2021/12/9 10:29
 * @Version 1.0
 */

public class LetterRank {
    private Integer id;
    private String userName;
    private Integer userScore;
    private Integer playTime;
    private Date createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getUserScore() {
        return userScore;
    }

    public void setUserScore(Integer userScore) {
        this.userScore = userScore;
    }

    public Integer getPlayTime() {
        return playTime;
    }

    public void setPlayTime(Integer playTime) {
        this.playTime = playTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
