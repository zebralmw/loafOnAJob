package com.lmw.config;

import com.lmw.utils.LoafOnAJobCacheUtil;
import com.lmw.youDrawIGuess.entity.Thesaurus;
import com.lmw.youDrawIGuess.repository.ThesaurusR;
import com.lmw.youDrawIGuess.util.CountDownUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author liumengwei
 * @Description TODO
 * @Date 2022/6/27 17:36
 * @Version 1.0
 */
@Slf4j
@Component
public class LoafOnAJobRunner implements ApplicationRunner {

    @Resource
    private ThesaurusR thesaurusR;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("==开始初始化你画我猜词库==");
        List<String> all = thesaurusR.queryValues();
        LoafOnAJobCacheUtil.initDrawThesaurus(all);
        log.info("==初始化你画我猜词库完成【{}】条==",all.size());

        CountDownUtil.initCountDownSchedule();
    }
}
