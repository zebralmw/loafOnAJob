package com.lmw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoafOnAJobApplication {

    public static void main(String[] args) {
        SpringApplication.run(LoafOnAJobApplication.class, args);
    }

}
