package com.lmw.ball.socket;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@ServerEndpoint(value = "/ball/{clientId}")
@Component
public class BallSocket{

    private static Map<String,Session> sessionMap = new ConcurrentHashMap<>();

    /**
     * 连接建立成功调用的方法
     */
    @OnOpen
    public void onOpen(@PathParam("clientId")String clientId, Session session) {
        System.out.println(clientId);
        sessionMap.put(clientId,session);
    }

    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose(Session session) {
    }

    /**
     * 收到客户端消息后调用的方法
     *
     * @param messageStr 客户端发送过来的消息
     */
    @OnMessage
    public void onMessage(String messageStr, Session session) {
//        JSONObject msg = JSONObject.parseObject(messageStr);
        try {
            sessionMap.get("B").getBasicRemote().sendText(messageStr);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @OnError
    public void onError(Session session, Throwable error) {
        log.error("发生错误");
        error.printStackTrace();
    }

}