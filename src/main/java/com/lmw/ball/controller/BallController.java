package com.lmw.ball.controller;

import cn.hutool.core.util.RandomUtil;
import com.lmw.animal.data.GamePlayerSocket;
import com.lmw.animal.socket.AnimalSocket;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

@Controller
@RequestMapping(("ball"))
public class BallController {
    //棋盘界面
    @GetMapping
    public String ball(Model model) {
        String s = RandomUtil.randomString(10);
        model.addAttribute("ID",s);
        return "ballGame";
    }

}
