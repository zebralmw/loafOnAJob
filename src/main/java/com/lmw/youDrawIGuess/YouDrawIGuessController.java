package com.lmw.youDrawIGuess;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.lang.UUID;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.lmw.utils.LoafOnAJobCacheUtil;
import com.lmw.utils.LoafOnAJobMailUtil;
import com.lmw.utils.Result;
import com.lmw.youDrawIGuess.entity.DrawUser;
import com.lmw.youDrawIGuess.entity.RegisterUser;
import com.lmw.youDrawIGuess.entity.User;
import com.lmw.youDrawIGuess.repository.DrawUserR;
import com.lmw.youDrawIGuess.security.SecurityCheck;
import com.lmw.youDrawIGuess.socket.SocketMessageHandler;
import com.lmw.youDrawIGuess.util.HttpSessionUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.*;

/**
 * @Author liumengwei
 * @Description TODO
 * @Date 2022/6/16 16:34
 * @Version 1.0
 */
@Slf4j
@Controller
@RequestMapping(("youDrawIGuess"))
public class YouDrawIGuessController {

    @Resource
    private DrawUserR drawUserR;


    @SecurityCheck
    @GetMapping("login")
    public Object login() {
        return "youDrawIGuess/login";
    }


    @PostMapping("login")
    @ResponseBody
    public Object login(@RequestBody User user) {
        DrawUser byEmail = drawUserR.findByEmail(user.getEmail());
        if (ObjectUtil.isNull(byEmail)){
            return Result.nok("用户不存在！");
        }
        if (!StrUtil.equals(byEmail.getPassword(),user.getPassword())){
            return Result.nok("密码不正确！");
        }
        BeanUtil.copyProperties(byEmail,user,true);
        HttpSessionUtil.login(user);
        return Result.ok();
    }

    @PostMapping("register")
    @ResponseBody
    public Object register(@RequestBody RegisterUser user) {
        DrawUser drawUser = new DrawUser();

        if (!StrUtil.equals(user.getPassword(),user.getConfirmPassword())){
            return Result.nok("密码不一致");
        }

        boolean checkCode = LoafOnAJobCacheUtil.checkCode(user.getEmail(), user.getCode());
        if (!checkCode){
            return Result.nok("验证码错误");
        }
        BeanUtil.copyProperties(user,drawUser,true);
        String userId = UUID.randomUUID(true).toString(true);
        drawUser.setUserId(userId);
        drawUser.setCreateTime(new Date());
        drawUser.setGameFrequency(0);
        drawUserR.save(drawUser);
        return Result.ok();
    }

    @PostMapping("sendCode")
    @ResponseBody
    public Result sendCode(@RequestBody User user) {
        DrawUser byEmail = drawUserR.findByEmail(user.getEmail());
        if (ObjectUtil.isNotNull(byEmail)){
            return Result.nok("该邮箱已存在！");
        }
        String code = RandomUtil.randomNumbers(4);
        System.out.println("邮箱验证码"+code);
        log.info("【{}】邮箱验证码【{}】",user.getEmail(),code);
        LoafOnAJobCacheUtil.setCodeCache(user.getEmail(),code);
        return LoafOnAJobMailUtil.sendCaptcha(user.getEmail(), code);
    }


    @GetMapping("roomList")
    @ResponseBody
    public Object roomList() {
        List<Room> roomList = RoomHall.getRoomList();
        List<Map<String,Object>> returnRooms = new ArrayList<>();
        for (Room room : roomList) {
            Map<String,Object> map = new HashMap<>();
            map.put("roomId",room.getRoomId());
            map.put("roomName",room.getRoomName());
            map.put("playerNum",room.getPlayerNum());
            map.put("players",room.getPlayers());
            returnRooms.add(map);
        }
        return Result.ok(returnRooms);
    }


    @PostMapping("createRoom")
    @ResponseBody
    public Object createRoom(@RequestBody Room room) {
        room.setRoomId(UUID.randomUUID(true).toString(true));
        User user = HttpSessionUtil.getUser();
        user.setRoomId(room.getRoomId());
        room.setCreatUser(user);
        HttpSessionUtil.login(user);
        RoomHall.addRoom(room);
        return Result.ok(room.getRoomId());
    }

    @PostMapping("joinRoom")
    @ResponseBody
    public Object joinRoom(@RequestBody Room room) {
        User user = HttpSessionUtil.getUser();
        Room joinRoom = RoomHall.getRoom(room.getRoomId());
        if (ObjectUtil.isEmpty(joinRoom)){
            return Result.nok("房间不存在");
        }
        if (StrUtil.equalsIgnoreCase(joinRoom.getPassword(),room.getPassword())){
            user.setRoomId(room.getRoomId());
            HttpSessionUtil.login(user);
            return Result.ok();
        }

        return Result.nok("密码错误");
    }


    @SecurityCheck
    @GetMapping("hall")
    public String goHall() {
        return HttpSessionUtil.checkAllowJump("youDrawIGuess/hall");
    }

    @GetMapping("room")
    public String goRoom(Model model){
        User user = HttpSessionUtil.getUser();
        if (ObjectUtil.isNotNull(user)){
            model.addAttribute("USER_ID",user.getUserId());
        }
        return HttpSessionUtil.checkAllowJump("youDrawIGuess/room");
    }

    @SecurityCheck
    @GetMapping("getRandomDrawThesaurus")
    @ResponseBody
    public Result<Set<String>> getRandomDrawThesaurus(){

        return Result.ok(LoafOnAJobCacheUtil.getRandomDrawThesaurus());
    }


    @PostMapping("chooseDrawThesaurus")
    @ResponseBody
    public Result<String> chooseDrawThesaurus(@RequestParam("drawThesaurus")String drawThesaurus){
        System.out.println(drawThesaurus);
        User user = HttpSessionUtil.getUser();
        User userCache = LoafOnAJobCacheUtil.getUserCache(user.getUserId());
        userCache.getPlayer().setIsChoose(true);
        Room room = RoomHall.getRoom(userCache.getRoomId());
        room.updateDrawingTargetAndChooseStatus(userCache.getUserId(),drawThesaurus);
        return Result.ok(drawThesaurus);
    }

    @PostMapping("playerReady")
    @ResponseBody
    public Result playerReady(){
        //玩家准备
        User user = HttpSessionUtil.getUser();
        User userCache = LoafOnAJobCacheUtil.getUserCache(user.getUserId());
        userCache.getPlayer().setIsReady(true);
        HttpSessionUtil.login(userCache);
        Room room = RoomHall.getRoom(userCache.getRoomId());
        room.updatePlayerReadyStatus(userCache.getUserId());
        return Result.ok();
    }

}
