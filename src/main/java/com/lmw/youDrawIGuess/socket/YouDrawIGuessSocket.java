package com.lmw.youDrawIGuess.socket;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.lmw.config.WebSocketConfig;
import com.lmw.utils.LoafOnAJobCacheUtil;
import com.lmw.utils.Result;
import com.lmw.youDrawIGuess.RoomHall;
import com.lmw.youDrawIGuess.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpSession;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

@Slf4j
@ServerEndpoint(value = "/youDrawIGuess/{userId}",configurator = WebSocketConfig.class)
@Component
public class YouDrawIGuessSocket{
    //当前玩家的session会话
    public Session session;

    /**
     * 连接建立成功调用的方法
     */
    @OnOpen
    public void onOpen(@PathParam("userId")String userId,
                       Session session) {
        User user = LoafOnAJobCacheUtil.getUserCache(userId);

        if (ObjectUtil.isEmpty(user)){
            log.info("连接异常，该用户未登录");
            send(session,JSON.toJSONString(Result.nok(302,"未登陆")));
            return;
        }
        this.session = session;
        System.out.println(JSON.toJSONString(user));
        RoomHall.joinRoom(user,user.getRoomId(),this);
        log.info("有新连接加入：{}", userId);
    }

    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose(Session session) {

        log.info("有一连接关闭：{}，当前在线人数为：{}", session.getId());
    }

    /**
     * 收到客户端消息后调用的方法
     *
     * @param messageStr 客户端发送过来的消息
     */
    @OnMessage
    public void onMessage(@PathParam("userId")String userId,String messageStr) {
        SocketMessageHandler.handler(userId,messageStr);
    }

    @OnError
    public void onError(Session session, Throwable error) {
        log.error("发生错误");
        error.printStackTrace();
    }



    private void send(Session session,String message){
        try {
            session.getBasicRemote().sendText(message);
        } catch (Exception e) {
            log.error("发生消息异常,sessionId->{};message->{}",session.getId(),message);
        }
    }

}