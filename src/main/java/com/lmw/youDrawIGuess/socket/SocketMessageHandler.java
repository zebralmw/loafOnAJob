package com.lmw.youDrawIGuess.socket;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.emoji.EmojiUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.lmw.utils.LoafOnAJobCacheUtil;
import com.lmw.youDrawIGuess.Player;
import com.lmw.youDrawIGuess.Room;
import com.lmw.youDrawIGuess.RoomHall;
import com.lmw.youDrawIGuess.entity.Message;
import com.lmw.youDrawIGuess.entity.User;
import lombok.Data;

import java.nio.charset.Charset;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author liumengwei
 * @Description TODO
 * @Date 2022/7/1 15:43
 * @Version 1.0
 */
public class SocketMessageHandler {

    static ThreadLocal<Info> contextInfo = new ThreadLocal<>();

    public static void handler(String userId,String messageStr){
        Info info = new Info();
        User userCache = LoafOnAJobCacheUtil.getUserCache(userId);
        Room room = RoomHall.getRoom(userCache.getRoomId());
        info.setRoom(room);
        info.setUser(userCache);
        String[] messageArr = StrUtil.splitToArray(messageStr, '-', 2);
        info.setMessage(messageArr[1]);

        contextInfo.set(info);
        Handler.valueOf(messageArr[0]).handler();
        contextInfo.remove();
    }

    public static void main(String[] args) {
        String s = "\uD83D\uDE00\uD83D\uDE03\uD83D\uDE04\uD83D\uDE01\uD83D\uDE06\uD83D\uDE05\uD83E\uDD23\uD83D\uDE02\uD83D\uDE42\uD83D\uDE43\uD83D\uDE09\uD83D\uDE0A\uD83D\uDE07\uD83E\uDD70\uD83D\uDE0D\uD83E\uDD29\uD83D\uDE18\uD83D\uDE17☺️\uD83D\uDE1A\uD83D\uDE19\uD83E\uDD72\uD83D\uDE0B\uD83D\uDE1B\uD83D\uDE1C\uD83E\uDD2A\uD83D\uDE1D\uD83E\uDD11\uD83E\uDD17\uD83E\uDD2D\uD83E\uDD2B\uD83E\uDD14\uD83E\uDD10\uD83E\uDD28\uD83D\uDE10\uD83D\uDE11\uD83D\uDE36\uD83D\uDE0F\uD83D\uDE12\uD83D\uDE44\uD83D\uDE2C\uD83E\uDD25\uD83D\uDE0C\uD83D\uDE14\uD83D\uDE2A\uD83E\uDD24\uD83D\uDE34\uD83D\uDE37\uD83E\uDD12\uD83E\uDD15\uD83E\uDD22\uD83E\uDD2E\uD83E\uDD27\uD83E\uDD75\uD83E\uDD76\uD83E\uDD74\uD83D\uDE35\uD83E\uDD2F\uD83E\uDD20\uD83E\uDD73\uD83E\uDD78\uD83D\uDE0E\uD83E\uDD13\uD83E\uDDD0\uD83D\uDE15\uD83D\uDE1F\uD83D\uDE41☹️\uD83D\uDE2E\uD83D\uDE2F\uD83D\uDE32\uD83D\uDE33\uD83E\uDD7A\uD83D\uDE26\uD83D\uDE27\uD83D\uDE28\uD83D\uDE30\uD83D\uDE25\uD83D\uDE22\uD83D\uDE2D\uD83D\uDE31\uD83D\uDE16\uD83D\uDE23\uD83D\uDE1E\uD83D\uDE13\uD83D\uDE29\uD83D\uDE2B\uD83E\uDD71\uD83D\uDE24\uD83D\uDE21\uD83D\uDE20\uD83E\uDD2C\n";
        List<String> list = EmojiUtil.extractEmojis(s);
        for (String s1 : list) {
            String s2 = EmojiUtil.toHtml(s1);
            System.out.println(s2);
            System.out.println(s1);
        }
    }








    public enum Handler{
        S1("S1","画板消息"){
            @Override
            void handler() {
                contextInfo.get().room.syncDrawingBoard(this.type +"-"+ contextInfo.get().message);
            }
        },

        S2("S2","聊天消息"){
            @Override
            void handler() {
                Message message = new Message();
                message.setContent(contextInfo.get().message);
                message.setUserId(contextInfo.get().getUser().getUserId());
                message.setDateTime(DateUtil.formatDateTime(new Date()));
                message.setNickName(contextInfo.get().getUser().getNickName());

                contextInfo.get().room.syncMessage(this.type +"-"+ Base64.encode(JSONUtil.toJsonStr(message), Charset.forName("UTF-8")));
            }
        },

        S3("S3","玩家动态"){
            @Override
            void handler() {
                Room room = contextInfo.get().getRoom();
                List<Player> players = room.getPlayers();
                String message = JSONUtil.toJsonStr(players);
                contextInfo.get().room.syncPlayerDynamic(this.type +"-"+ Base64.encode(JSONUtil.toJsonStr(message), Charset.forName("UTF-8")));
            }
        },
        S4("S4","房间信息"){
            @Override
            void handler() {
                Room room = contextInfo.get().getRoom();

                Map<String,Object> map = new HashMap<>();
                map.put("isStart",room.getIsStart());
                map.put("isStart",room.getIsStart());

                String message = JSONUtil.toJsonStr(map);
                contextInfo.get().room.syncRoomDynamic(this.type +"-"+ Base64.encode(JSONUtil.toJsonStr(message), Charset.forName("UTF-8")));
            }
        },
        S5("S5","倒计时信息"){
            @Override
            void handler() {
                String message = contextInfo.get().getMessage();
                contextInfo.get().room.syncCountDown(this.type +"-"+ Base64.encode(message));
                //当前玩家绘画时间结束
                if (StrUtil.equals(message,"0")){
                    contextInfo.get().room.switchPlayer();
                }
            }
        },
        S6("S6","绘画开始"){
            @Override
            void handler() {
                String message = contextInfo.get().getMessage();
                contextInfo.get().room.syncDrawStart(this.type +"-"+ Base64.encode(message));
            }
        };

        public String type;
        public String typeName;
        Handler(String type,String typeName){
            this.type = type;
            this.typeName = typeName;
        }
        abstract void handler();
    }


    @Data
    static class Info{
        private User user;
        private String message;
        private Room room;
    }
}
