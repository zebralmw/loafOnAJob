package com.lmw.youDrawIGuess.security;

import cn.hutool.core.util.StrUtil;
import com.lmw.utils.Result;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * @Author liumengwei
 * @Description TODO
 * @Date 2022/6/28 17:32
 * @Version 1.0
 */
@Aspect
@Component
public class SecurityAop {


    @Pointcut("@annotation(com.lmw.youDrawIGuess.security.SecurityCheck)")
    public void pointCut(){};


    @Around("pointCut()")
    public Object around(ProceedingJoinPoint pjp) throws Throwable{
        System.out.println("beginning----");
        Object object = pjp.proceed();    //运行doSth()，返回值用一个Object类型来接收
//        if (object instanceof String && !StrUtil.equals(String.valueOf(object),"youDrawIGuess/login")){
//            System.out.println(object);
//            return "redirect:login";
//        }
        System.out.println("ending----");
        return object;
    }
}
