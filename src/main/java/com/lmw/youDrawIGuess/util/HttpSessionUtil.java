package com.lmw.youDrawIGuess.util;


import cn.hutool.core.util.ObjectUtil;
import com.lmw.utils.LoafOnAJobCacheUtil;
import com.lmw.youDrawIGuess.entity.User;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Objects;

public class HttpSessionUtil {


    private static HttpSession getHttpSession() {
        HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes()))
                .getRequest();
        return request.getSession();
    }

    public static User getUser(){
        return (User) getHttpSession().getAttribute("user");
    }

    public static void login(User user){
        LoafOnAJobCacheUtil.setUserCache(user);
        getHttpSession().setAttribute("user",user);
    }


    public static String checkAllowJump(String toPage){
        User user = (User) getHttpSession().getAttribute("user");
        if (ObjectUtil.isNotEmpty(user)){
            return toPage;
        }
        return "redirect:login";
    }
}
