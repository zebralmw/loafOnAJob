package com.lmw.youDrawIGuess.util;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.cron.CronUtil;
import cn.hutool.cron.task.Task;
import com.lmw.youDrawIGuess.Room;
import com.lmw.youDrawIGuess.socket.SocketMessageHandler;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author liumengwei
 * @Description TODO
 * @Date 2022/7/5 16:21
 * @Version 1.0
 */
@Slf4j
public class CountDownUtil {

    public volatile static Long DEFAULT_TIME = 10l;

    public volatile static Long COUNT = 0l;

    public volatile static Map<Long, List<String>> COUNT_DOWN_LIST = new ConcurrentHashMap<>();


    public synchronized static void addCountDown(String userId){
        Long key = Long.valueOf(DEFAULT_TIME + COUNT);
        List<String> orDefault = COUNT_DOWN_LIST.getOrDefault(key, new ArrayList<>());
        orDefault.add(userId);
        COUNT_DOWN_LIST.put(key,orDefault);
    }

    public static void initCountDownSchedule(){
        CronUtil.schedule("*/1 * * * * *", new Task() {
            @Override
            public void execute() {
                COUNT++;
                Set<Long> longs = COUNT_DOWN_LIST.keySet();

                for (Long aLong : longs) {
                    List<String> userIds = COUNT_DOWN_LIST.get(aLong);

                    if (CollectionUtil.isNotEmpty(userIds)){
                        for (String userId : userIds) {
                            SocketMessageHandler.handler(userId, SocketMessageHandler.Handler.S5.type + "-" + (aLong - COUNT));
                        }
                    }
                    if (aLong.equals(COUNT)){
                        COUNT_DOWN_LIST.remove(aLong);
                    }
                }

            }
        });
        CronUtil.setMatchSecond(true);
        CronUtil.start();
        log.info("==倒计时监听 启动==");
    }
}
