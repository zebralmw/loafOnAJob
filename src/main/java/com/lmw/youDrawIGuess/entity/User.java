package com.lmw.youDrawIGuess.entity;

import com.lmw.youDrawIGuess.Player;
import lombok.Data;

import java.beans.Transient;

/**
 * @Author liumengwei
 * @Description TODO
 * @Date 2022/6/17 15:38
 * @Version 1.0
 */
@Data
public class User extends DrawUser{

    private Player player;

    private String roomId;

    @Transient
    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }
}
