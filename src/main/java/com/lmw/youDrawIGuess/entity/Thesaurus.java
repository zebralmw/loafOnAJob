package com.lmw.youDrawIGuess.entity;

import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.digest.MD5;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @Author liumengwei
 * @Description TODO
 * @Date 2022/6/27 15:26
 * @Version 1.0
 */
@Entity
@Data
public class Thesaurus {

    @Id
    @Column(length = 32,columnDefinition = "VARCHAR(32) COMMENT '值的MD5'")
    private String id;

    @Column(length = 255,columnDefinition = "VARCHAR(255) COMMENT '值'")
    private String value;

    @Column(length = 255,columnDefinition = "VARCHAR(255) COMMENT '提示语'")
    private String remind;

    @Column(length = 11,columnDefinition = "INT(11) COMMENT '选择次数'")
    private Integer chooseFrequency;

    @Column(length = 11,columnDefinition = "INT(11) COMMENT '回答正确次数'")
    private Integer successFrequency;

    @Column(length = 11,columnDefinition = "INT(11) COMMENT '回答错误次数'")
    private Integer errorFrequency;

    @Column(length = 1,columnDefinition = "INT(1) COMMENT '状态'")
    private Integer status;

    public static void main(String[] args) {
        String s = MD5.create().digestHex("c4ca4238a0b923820dcc509a6f423475849bc4ca4238a0b923820dcc509a6f75849bc4ca4238a0b923820dcc509a6f75849b");
        System.out.println(s);
    }
}
