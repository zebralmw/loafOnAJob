package com.lmw.youDrawIGuess.entity;

import com.lmw.youDrawIGuess.Player;
import lombok.Data;

/**
 * @Author liumengwei
 * @Description TODO
 * @Date 2022/6/17 15:38
 * @Version 1.0
 */
@Data
public class RegisterUser extends DrawUser{
    private String code;

    private String confirmPassword;
}
