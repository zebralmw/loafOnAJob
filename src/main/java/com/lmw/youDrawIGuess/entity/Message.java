package com.lmw.youDrawIGuess.entity;

import lombok.Data;

/**
 * @Author liumengwei
 * @Description TODO
 * @Date 2022/6/17 10:12
 * @Version 1.0
 */
@Data
public class Message {
    private String userId;

    private String nickName;
    //消息内容
    private String content;
    //是否命中绘画目标
    private boolean isHitDrawingTarget;

    private String dateTime;
}
