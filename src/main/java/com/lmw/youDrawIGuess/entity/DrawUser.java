package com.lmw.youDrawIGuess.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

/**
 * (DrawUser)实体类
 *
 * @author liu meng wei
 * @since 2022-06-20 09:33:18
 */
@Entity
@Data
public class DrawUser implements Serializable {
    private static final long serialVersionUID = 694334492812013633L;

    @Id
    private String userId;

    @Column(length = 255)
    private String nickName;

    @Column(length = 255)
    private String email;

    @Column(length = 255)
    private String password;

    @Column
    private Date createTime;
    /**
     * 最后一次登录
     */
    @Column
    private Date lastLoginTime;
    /**
     * 游戏场次
     */
    @Column(length = 11)
    private Integer gameFrequency;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public Integer getGameFrequency() {
        return gameFrequency;
    }

    public void setGameFrequency(Integer gameFrequency) {
        this.gameFrequency = gameFrequency;
    }

}

