package com.lmw.youDrawIGuess.repository;

import com.lmw.youDrawIGuess.entity.DrawUser;
import com.lmw.youDrawIGuess.entity.Thesaurus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author liumengwei
 * @Description TODO
 * @Date 2022/6/27 16:12
 * @Version 1.0
 */
@Repository
public interface ThesaurusR extends JpaRepository<Thesaurus,String> {
    @Query(value = "select value from thesaurus",nativeQuery = true)
    List<String> queryValues();
}
