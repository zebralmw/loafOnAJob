package com.lmw.youDrawIGuess.repository;

import com.lmw.youDrawIGuess.entity.DrawUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @Author liumengwei
 * @Description TODO
 * @Date 2022/6/20 17:06
 * @Version 1.0
 */
@Repository
public interface DrawUserR extends JpaRepository<DrawUser,String> {
    DrawUser findByEmail(String email);
}