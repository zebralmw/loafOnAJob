package com.lmw.youDrawIGuess;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import com.lmw.youDrawIGuess.entity.Message;
import com.lmw.youDrawIGuess.entity.User;
import com.lmw.youDrawIGuess.socket.SocketMessageHandler;
import com.lmw.youDrawIGuess.util.CountDownUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

import java.beans.Transient;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Stack;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @Author liumengwei
 * @Description TODO 游戏房间
 * @Date 2022/6/17 9:10
 * @Version 1.0
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Room {

    private String roomId;

    private String roomName;

    private String password;

    private User creatUser;
    //存放画板内容
    private Stack<String[]> drawingBoard = new Stack<>();

    //绘画目标
    private String drawingTarget;

    //玩家 第一位是房主，后面的是按照加入到房间先后顺序
    private List<Player> players = new CopyOnWriteArrayList<>();

    //玩家数量
    private Integer playerNum = 0;

    //画家位置
    private Integer painterPosition = 0;
    //画家用户ID
    private String painterUserId;

    private Boolean isStart = false;

    //消息列表
    private List<Message> messages = new CopyOnWriteArrayList<>();


    public void updateDrawingTargetAndChooseStatus(String userId,String drawingTarget){
        //判断当前画家用户ID是否一致
        if (!StrUtil.equals(this.painterUserId,userId)){
            return;
        }
        this.drawingTarget = drawingTarget;
        players.get(painterPosition).setIsChoose(true);
        //广播玩家动态
        SocketMessageHandler.handler(userId, SocketMessageHandler.Handler.S3.type + "-syncPlayerDynamic");
        //绘画开始消息广播
        SocketMessageHandler.handler(userId, SocketMessageHandler.Handler.S6.type + "-syncDrawStart");
        //开启倒计时
        CountDownUtil.addCountDown(userId);
    }

    public void updatePlayerReadyStatus(String userId){
        if (isStart){
            return;
        }
        boolean flag = true;
        for (Player player : players) {
            if (StrUtil.equals(player.getUserId(),userId)){
                player.setIsReady(true);
            }
            if (!player.getIsReady()){
                flag = false;
            }
        }
        //全部玩家准备就绪
        if (flag){
            setIsStart(true);
            players.get(painterPosition).setIsDraw(true);
            setPainterUserId(players.get(painterPosition).getUserId());
        }
        //广播玩家动态
        SocketMessageHandler.handler(userId, SocketMessageHandler.Handler.S3.type + "-syncPlayerDynamic");
        //广播房间信息到房间内所有玩家
        SocketMessageHandler.handler(userId, SocketMessageHandler.Handler.S4.type + "-syncRoomDynamic");
    }



    public void switchPlayer() {
        players.get(painterPosition).setIsDraw(false);
        players.get(painterPosition).setIsChoose(false);
        if (this.painterPosition == (playerNum-1)){
            this.painterPosition = 0;
        }else{
            this.painterPosition++;
        }
        players.get(painterPosition).setIsDraw(true);
        setPainterUserId(players.get(painterPosition).getUserId());

        //广播玩家动态
        SocketMessageHandler.handler(getPainterUserId(), SocketMessageHandler.Handler.S3.type + "-syncPlayerDynamic");
        //广播房间信息到房间内所有玩家
        SocketMessageHandler.handler(getPainterUserId(), SocketMessageHandler.Handler.S4.type + "-syncRoomDynamic");
    }

    public synchronized boolean addPlayer(Player addPlayer){
        //刷新页面，重置websocket
        for (Player player : players) {
            if (StrUtil.equals(player.getUserId(),addPlayer.getUserId())){
                player.setSelfSocket(addPlayer.getSelfSocket());
                continue;
            }
        }
        if (playerNum <= players.size()){
            return false;
        }
        players.add(addPlayer);
        return true;
    }




    @SneakyThrows
    public void syncDrawingBoard(String point){
        for (int i = 0; i < players.size(); i++) {
            if (i == painterPosition){
                continue;
            }
            players.get(i).getSelfSocket().session.getBasicRemote().sendText(point);
        }
    }


    @SneakyThrows
    public void syncMessage(String message){
        for (int i = 0; i < players.size(); i++) {
            players.get(i).getSelfSocket().session.getBasicRemote().sendText(message);
        }
    }


    @SneakyThrows
    public void syncPlayerDynamic(String message){
        for (int i = 0; i < players.size(); i++) {
            players.get(i).getSelfSocket().session.getBasicRemote().sendText(message);
        }
    }

    @SneakyThrows
    public void syncRoomDynamic(String message) {
        for (int i = 0; i < players.size(); i++) {
            players.get(i).getSelfSocket().session.getBasicRemote().sendText(message);
        }
    }


    @SneakyThrows
    public void syncCountDown(String countDown) {
        for (int i = 0; i < players.size(); i++) {
            players.get(i).getSelfSocket().session.getBasicRemote().sendText(countDown);
        }
    }


    @SneakyThrows
    public void syncDrawStart(String message) {
        for (int i = 0; i < players.size(); i++) {
            players.get(i).getSelfSocket().session.getBasicRemote().sendText(message);
        }
    }

    @Transient
    public User getCreatUser() {
        return creatUser;
    }

    public void setCreatUser(User creatUser) {
        this.creatUser = creatUser;
    }


}
