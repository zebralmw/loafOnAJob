package com.lmw.youDrawIGuess;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.lmw.youDrawIGuess.entity.User;
import com.lmw.youDrawIGuess.socket.SocketMessageHandler;
import com.lmw.youDrawIGuess.socket.YouDrawIGuessSocket;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author liumengwei
 * @Description TODO 房间列表
 * @Date 2022/6/17 10:57
 * @Version 1.0
 */
public class RoomHall {
    public static void main(String[] args) {
        String code = "411104106";
        code = code.substring(0, 5) + "0000";
        System.out.println(code);
    }


    private volatile static Map<String,Room> ROOMS = new ConcurrentHashMap<>();

    private volatile static Map<String,String> USERID_SOCKET_SESSION_ID = new ConcurrentHashMap<>();


    public static List<Room> getRoomList(){
        return new ArrayList<>(ROOMS.values());
    }

    public static Room getRoom(String roomId){
       return ROOMS.get(roomId);
    }

    public static void addRoom(Room room){
        ROOMS.put(room.getRoomId(),room);
    }

    public static void joinRoom(User user, String roomId, YouDrawIGuessSocket youDrawIGuessSocket){
        Room room = getRoom(roomId);
        Player player = new Player();
        player.setIsRoomMaster(false);
        //判断是否是房主
        if (StrUtil.equalsIgnoreCase(room.getCreatUser().getUserId(),user.getUserId())){
            player.setIsRoomMaster(true);
        }
        player.setNickName(user.getNickName());
        player.setCurrentRoom(room);
        player.setUserId(user.getUserId());
        player.setSelfSocket(youDrawIGuessSocket);
        user.setPlayer(player);
        room.addPlayer(player);
        //广播玩家动态
        SocketMessageHandler.handler(user.getUserId(), SocketMessageHandler.Handler.S3.type + "-syncPlayerDynamic");
    }


}
