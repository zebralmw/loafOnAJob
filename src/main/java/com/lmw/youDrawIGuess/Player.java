package com.lmw.youDrawIGuess;

import com.lmw.youDrawIGuess.entity.User;
import com.lmw.youDrawIGuess.socket.YouDrawIGuessSocket;
import lombok.Data;

import java.beans.Transient;

/**
 * @Author liumengwei
 * @Description TODO 玩家
 * @Date 2022/6/17 9:11
 * @Version 1.0
 */
@Data
public class Player{
    private String userId;

    private String nickName;

    private YouDrawIGuessSocket selfSocket;

    private Room currentRoom;

    //是否为房主
    private Boolean isRoomMaster;

    //是否准备就绪
    private Boolean isReady = false;

    //是否为画家
    private Boolean isDraw = false;

    //是否选择绘画目标
    private Boolean isChoose = false;



    @Transient
    public Room getCurrentRoom() {
        return currentRoom;
    }

    public void setCurrentRoom(Room currentRoom) {
        this.currentRoom = currentRoom;
    }

    @Transient
    public YouDrawIGuessSocket getSelfSocket() {
        return selfSocket;
    }

    public void setSelfSocket(YouDrawIGuessSocket selfSocket) {
        this.selfSocket = selfSocket;
    }
}
