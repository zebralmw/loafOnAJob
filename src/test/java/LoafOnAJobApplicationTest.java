import cn.hutool.core.collection.LineIter;
import cn.hutool.core.io.resource.ResourceUtil;
import cn.hutool.crypto.digest.MD5;
import com.lmw.LoafOnAJobApplication;
import com.lmw.youDrawIGuess.entity.Thesaurus;
import com.lmw.youDrawIGuess.repository.ThesaurusR;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author liumengwei
 * @Description TODO
 * @Date 2022/6/27 16:11
 * @Version 1.0
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = LoafOnAJobApplication.class,webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class LoafOnAJobApplicationTest {
    @Resource
    private ThesaurusR thesaurusR;
    static MD5 md5 = MD5.create();
    @Test
    public void test(){
        List<Thesaurus> list = new ArrayList<>();
        final LineIter lineIter = new LineIter(ResourceUtil.getUtf8Reader("E:\\ceshi\\你画我猜词汇.txt"));
        for (String line : lineIter) {
            String[] split = line.split("：");
            String title = split[0];
            String values = split[1];
            String[] valueArr = values.split("，");
            for (String value : valueArr) {
                Thesaurus thesaurus = new Thesaurus();
                thesaurus.setId(md5.digestHex(value));
                thesaurus.setValue(value);
                thesaurus.setRemind(title);
                thesaurus.setChooseFrequency(0);
                thesaurus.setErrorFrequency(0);
                thesaurus.setSuccessFrequency(0);
                list.add(thesaurus);
            }

        }
        thesaurusR.saveAll(list);
    }

}
