package com.lmw.gobang;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.Set;

@SpringBootTest
class GobangApplicationTests {

    @Test
    void contextLoads() {
    }

    public static final Integer A = 30;
    public static final Integer HOW_LINE = 19;
    public static final String WHAT_SIDE = "white";
    public static HashMap<String,String> allPoint = new HashMap<>();


    public static void main(String[] args) {
        Integer[] X = new Integer[HOW_LINE];
        Integer[] Y = new Integer[HOW_LINE];
        for (int i = 1; i <= HOW_LINE; i++) {
            X[i-1] = i*A;
            Y[i-1] = i*A;
        }
        for (Integer x : X) {
            for (Integer y : Y) {
                allPoint.put(x+"-"+y,WHAT_SIDE);
            }
        }
        allPoint.put("240-150","black");
        allPoint.put("240-300","black");
        allPoint.put("330-180","black");
        allPoint.put("150-180","black");
        allPoint.put("210-150","black");
        allPoint.put("210-210","black");
        allPoint.put("270-150","black");
        allPoint.put("270-210","black");
//        leftOblique(240,180);
        rightOblique(240,180);
    }













    /**
     * 计算 左斜方向是否有五子相连
     * @param x
     * @param y
     */
    private static void leftOblique(Integer x,Integer y){
        //计算当前点的左上方 有几个连贯的当前点的颜色
        Integer leftTop = 0;
        for (int i = 1; i < 5; i++) {
            if (x-A*i == 0){
                break;
            }
            if (y-A*i == 0){
                break;
            }
            String winLine = x-A*i + "-" + (y-A*i);
            if (!WHAT_SIDE.equals(allPoint.get(winLine))){
                break;
            }
            leftTop = i;
        }
        if ( leftTop == 4 ){
            //当前下棋者胜利
            System.out.println("当前下棋者胜利");
            return;
        }

        //计算当前点的左下方 有几个连贯的当前点的颜色
        Integer leftBottom = 0;
        for (int i = 1; i < 5; i++) {
            if (x-A*i == 0){
                break;
            }
            if (y+A*i == A*HOW_LINE){
                break;
            }
            String winLine = x-A*i + "-" + (y+A*i);
            if (!WHAT_SIDE.equals(allPoint.get(winLine))){
                break;
            }
            leftBottom = i;
        }
        if ( leftBottom == 4 ){
            //当前下棋者胜利
            System.out.println("当前下棋者胜利");
            return;
        }

        if (leftBottom + leftTop >= 4){
            //当前下棋者胜利
            System.out.println("当前下棋者胜利");
            return;
        }
    }


    /**
     * 计算 右斜方向是否有五子相连
     * @param x
     * @param y
     */
    private static void rightOblique(Integer x,Integer y){
        //计算当前点的右上方 有几个连贯的当前点的颜色
        Integer rightTop = 0;
        for (int i = 1; i < 5; i++) {
            if (x-A*i==0){
                break;
            }
            if (y+A*i==A*HOW_LINE){
                break;
            }
            String winLine = x-A*i + "-" + (y+A*i);
            if (!WHAT_SIDE.equals(allPoint.get(winLine))){
                break;
            }
            rightTop = i;
        }
        if ( rightTop == 4 ){
            //当前下棋者胜利
            System.out.println("当前下棋者胜利");
            return;
        }

        //计算当前点的右下方 有几个连贯的当前点的颜色
        Integer rightBottom = 0;
        for (int i = 1; i < 5; i++) {
            if (x+A*i==A*HOW_LINE){
                break;
            }
            if (y+A*i==A*HOW_LINE){
                break;
            }
            String winLine = x+A*i + "-" + (y+A*i);
            if (!WHAT_SIDE.equals(allPoint.get(winLine))){
                break;
            }
            rightBottom = i;
        }
        if ( rightBottom == 4 ){
            //当前下棋者胜利
            System.out.println("当前下棋者胜利");
            return;
        }

        if (rightBottom + rightTop >= 4){
            //当前下棋者胜利
            System.out.println("当前下棋者胜利");
            return;
        }
    }


    /**
     * 计算 水平方向是否有五子相连
     * @param x
     * @param y
     */
    private static void level(Integer x,Integer y){
        //计算当前点的左方 有几个连贯的当前点的颜色
        Integer left = 0;
        for (int i = 1; i < 5; i++) {
            if (x-A*i==0){
                break;
            }
            String winLine = x-A*i + "-" + y;
            if (!WHAT_SIDE.equals(allPoint.get(winLine))){
                break;
            }
            left = i;
        }
        if ( left == 4 ){
            //当前下棋者胜利
            System.out.println("当前下棋者胜利");
            return;
        }

        //计算当前点的右方 有几个连贯的当前点的颜色
        Integer right = 0;
        for (int i = 1; i < 5; i++) {
            if (x+A*i==A*HOW_LINE){
                break;
            }
            String winLine = x+A*i + "-" + y;
            if (!WHAT_SIDE.equals(allPoint.get(winLine))){
                break;
            }
            right = i;
        }
        if ( right == 4 ){
            //当前下棋者胜利
            System.out.println("当前下棋者胜利");
            return;
        }

        if (right + left >= 4){
            //当前下棋者胜利
            System.out.println("当前下棋者胜利");
            return;
        }
    }

    /**
     * 计算 垂直方向是否有五子相连
     * @param x
     * @param y
     */
    private static void vertical(Integer x,Integer y){
        //计算当前点的上方 有几个连贯的当前点的颜色
        Integer top = 0;
        for (int i = 1; i < 5; i++) {
            if (y-A*i==0){
                break;
            }
            String winLine = x + "-" +( y-A*i );
            if (!WHAT_SIDE.equals(allPoint.get(winLine))){
                break;
            }
            top = i;
        }
        if ( top == 4 ){
            //当前下棋者胜利
            System.out.println("当前下棋者胜利");
            return;
        }

        //计算当前点的下方 有几个连贯的当前点的颜色
        Integer bottom = 0;
        for (int i = 1; i < 5; i++) {
            if (y+A*i==A*HOW_LINE){
                break;
            }
            String winLine = x + "-" +( y+A*i );
            if (!WHAT_SIDE.equals(allPoint.get(winLine))){
                break;
            }
            bottom = i;
        }
        if ( bottom == 4 ){
            //当前下棋者胜利
            System.out.println("当前下棋者胜利");
            return;
        }

        if (bottom + top >= 4){
            //当前下棋者胜利
            System.out.println("当前下棋者胜利");
            return;
        }
    }
}
